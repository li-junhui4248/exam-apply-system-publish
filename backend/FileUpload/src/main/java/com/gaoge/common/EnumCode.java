package com.gaoge.common;

/**
 * 规定枚举字段接口
 *
 * @author gaoge
 * @since 2021-11-07 13:42:39
 */
public interface EnumCode {
    long getCode();

    String getMsg();


}
